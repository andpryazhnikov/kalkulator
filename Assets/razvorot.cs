using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class razvorot : MonoBehaviour
{
    public GameObject go;
    public Text text;
    public string copyText;
    public string znak;
    public string[] simvl = {"%","7","8","9","/","√","4","5","6","*","<-","1","2","3","-","CE","0",",","=","+"};
    // Start is called before the first frame update
    void Start()
    {
        int schetchik = 0;
        for (int i = 1; i < 5; i++){
            for (int j = 1; j < 6; j++){
                GameObject goC = Instantiate(go);
                goC.transform.SetParent(go.transform.parent, true);
                goC.transform.localScale = go.transform.localScale;
                goC.transform.localPosition = go.transform.localPosition + new Vector3(40.0f * j,-40f * i ,0);
                goC.transform.GetChild(0).gameObject.GetComponent<Text>().text = simvl[schetchik];
                schetchik++;
            }
        }
        Destroy(go.gameObject);
    }

    public void ChisloNull(Text vibor){
        if (vibor.text == "+"){
            copyText = text.text;
            text.text = "";
            znak = "+";
        }
        else if (vibor.text == "-"){
            copyText = text.text;
            text.text = "";
            znak = "-";
        }
        else if (vibor.text == "*"){
            copyText = text.text;
            text.text = "";
            znak = "*";
        }
        else if (vibor.text == "/"){
            copyText = text.text;
            text.text = "";
            znak = "/";
        }
        else if (vibor.text == "="){
            switch (znak){
                case "+":
                    text.text = Convert.ToString(Convert.ToDouble(copyText) + Convert.ToDouble(text.text));
                    break;
                case "-":
                    text.text = Convert.ToString(Convert.ToDouble(copyText) - Convert.ToDouble(text.text));
                    break;
                case "*":
                    text.text = Convert.ToString(Convert.ToDouble(copyText) * Convert.ToDouble(text.text));
                    break;
                case "/":
                    text.text = Convert.ToString(Convert.ToDouble(copyText) / Convert.ToDouble(text.text));
                    break;
                case "%":
                    text.text = Convert.ToString(Convert.ToDouble(copyText) % Convert.ToDouble(text.text));
                    break;
            }   
        }
        else if (vibor.text == "CE"){
            text.text = "";
            copyText = "";
        }
        else if (vibor.text == "<-"){
            text.text = text.text.Substring(0, text.text.Length - 1);
        }
        else if (vibor.text == "√"){
            text.text = Convert.ToString(Math.Sqrt(Convert.ToDouble(text.text)));
        }
        else if (vibor.text == "%"){
            copyText = text.text;
            text.text = "";
            znak = "%";
        }
        else{
            text.text += vibor.text;
        }
    }
}
